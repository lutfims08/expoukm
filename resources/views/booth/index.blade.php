<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="src/styles.css">
    {{--  <iframe allowfullscreen src="https%3A%2F%2Fpanorama-panoramabucket-17m6l0kcpd6x2.s3.eu-central-1.amazonaws.com%2F167ff8a5-a4ab-4d7e-9bff-6dc810fc5cf5.jpeg%3FX-Amz-Expires%3D21600%26x-amz-security-token%3DIQoJb3JpZ2luX2VjEJT%252F%252F%252F%252F%252F%252F%252F%252F%252F%252FwEaDGV1LWNlbnRyYWwtMSJGMEQCIEovnITkcj8xg3TU49qj1SysNyY%252BidL6d5UXpw%252BUXoFBAiAlxHltj4ZwS7xDwQOp5IQJ4baQd8VO9LspC12NOwNFCyrwAQjN%252F%252F%252F%252F%252F%252F%252F%252F%252F%252F8BEAEaDDY1MzM2MDA1MDkyNCIM0HypJTIHHw3qzhnOKsQB%252FUasYYJSzdhRjoWSpFgF8rsuCBRBJZGM8glqu783QD%252BBm7ELm1BABiu2p0NXBnZJywMcSM3ST2l1qqDpqTMPLx42cLNqJ%252BGIs9aCG3Y%252FDJn5czNm3Fj6R79rx9qwtrHJdCYlbhZ0N2GsCY9bdssyfpkGhSeu7JrRjIhl06d6cluPFFAs%252FTbICpw9RR74EjE7JOkEYnq9SyYtkbAHKZCB4o71eX2xcXDUCL7F15ZrgwVzOmjCYVCiUZbNpMP7kYBUTamuxDD%252Bkp%252F8BTrhAVKNgmGs4A0VyTDB2vzprp0UTdJGvf2l81xxVFVNdl3Hd6c3ychIZcibOxBnJF7euyTdyAV88kCsAuVU8y4TKnUQJ7R0fROYXupAvdO3oTYfchfBAdEuIXLDK62B5GkATJkGeUEkG%252BXG2ok6v899hT3swqVd%252BZ5XR%252FqUOBbQEEDJkqGYenilnyeGGEYgwf2z%252F1PSy4Ij5ZMSQw%252B15ti5xfbOpD%252BWUboiXciQ591vJrqygRXPabG8qqG3zlVzg09aru0QmxdS30Kn2Pog%252Bh7oHKicC1YIoI9yALT%252F0BaTtaJUAA%253D%253D%26X-Amz-Algorithm%3DAWS4-HMAC-SHA256%26X-Amz-Credential%3DASIAZQH2LGLWLHSSJTYZ%2F20201015%2Feu-central-1%2Fs3%2Faws4_request%26X-Amz-Date%3D20201015T040827Z%26X-Amz-SignedHeaders%3Dhost%3Bx-amz-security-token%26X-Amz-Signature%3D6ccfb2670eaf965175aecbca19fceda81735a5f0b2214c1e37c82be11254da36&is_stereo=false"></iframe>  --}}
    <script src="src/bundle.js"></script>
    <title>Enscape</title>
  </head>
  <body>
    <div id="splash">
      <div class="loading"></div>
    </div>
    <a-scene device-orientation-permission-ui="enabled: true" vr-mode-ui="enabled: true" loading-screen="dotsColor: #ef9604; backgroundColor: black" mobile-controls>
      <a-assets>
        <img id='default-image' material="color: black" src="https://panorama-panoramabucket-17m6l0kcpd6x2.s3.eu-central-1.amazonaws.com/167ff8a5-a4ab-4d7e-9bff-6dc810fc5cf5.jpeg?X-Amz-Expires=21600&x-amz-security-token=IQoJb3JpZ2luX2VjEJT%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDGV1LWNlbnRyYWwtMSJGMEQCIEovnITkcj8xg3TU49qj1SysNyY%2BidL6d5UXpw%2BUXoFBAiAlxHltj4ZwS7xDwQOp5IQJ4baQd8VO9LspC12NOwNFCyrwAQjN%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAEaDDY1MzM2MDA1MDkyNCIM0HypJTIHHw3qzhnOKsQB%2FUasYYJSzdhRjoWSpFgF8rsuCBRBJZGM8glqu783QD%2BBm7ELm1BABiu2p0NXBnZJywMcSM3ST2l1qqDpqTMPLx42cLNqJ%2BGIs9aCG3Y%2FDJn5czNm3Fj6R79rx9qwtrHJdCYlbhZ0N2GsCY9bdssyfpkGhSeu7JrRjIhl06d6cluPFFAs%2FTbICpw9RR74EjE7JOkEYnq9SyYtkbAHKZCB4o71eX2xcXDUCL7F15ZrgwVzOmjCYVCiUZbNpMP7kYBUTamuxDD%2Bkp%2F8BTrhAVKNgmGs4A0VyTDB2vzprp0UTdJGvf2l81xxVFVNdl3Hd6c3ychIZcibOxBnJF7euyTdyAV88kCsAuVU8y4TKnUQJ7R0fROYXupAvdO3oTYfchfBAdEuIXLDK62B5GkATJkGeUEkG%2BXG2ok6v899hT3swqVd%2BZ5XR%2FqUOBbQEEDJkqGYenilnyeGGEYgwf2z%2F1PSy4Ij5ZMSQw%2B15ti5xfbOpD%2BWUboiXciQ591vJrqygRXPabG8qqG3zlVzg09aru0QmxdS30Kn2Pog%2Bh7oHKicC1YIoI9yALT%2F0BaTtaJUAA%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAZQH2LGLWLHSSJTYZ/20201015/eu-central-1/s3/aws4_request&X-Amz-Date=20201015T040827Z&X-Amz-SignedHeaders=host;x-amz-security-token&X-Amz-Signature=6ccfb2670eaf965175aecbca19fceda81735a5f0b2214c1e37c82be11254da36"/>
      </a-assets>
      <!-- 360-degree image. -->
      <a-sky id="sky-left" segments-height="128" segments-width="128" radius="150" src="#default-image" reduce-segments-on-mobile="segments: 64"></a-sky>
      <a-sky id="sky-right" segments-height="128" segments-width="128" radius="150" src="#default-image" reduce-segments-on-mobile="segments: 64"></a-sky>
      <!-- Camera + cursor. -->
      <a-camera position="0 0 0" stereocam="eye: left" reverse-mouse-drag="true">
        <a-cursor id="cursor"
          scale="0.5 0.5 1"
          color="#ef9604"
          visible="false";
          event-set__1="_event: mouseenter; color: white"
          event-set__2="_event: mouseleave; color: #ef9604"
          raycaster="objects: .link">
          </a-cursor>
      </a-camera>
    </a-scene>
  </body>
</html>
