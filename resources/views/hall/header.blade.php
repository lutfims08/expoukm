<header id="header" >
    <div class="container">
        <div class="row align-items-center">
        <div class="col-6 col-md-3">
            <div class="logo">
            <img src="/assets/images/logo.png" alt="BCA" class="logo-white">
            <img src="/assets/images/logo.png" alt="BCA" class="logo-blue">
            </div>
        </div>
        <div class="col-6 col-md-9">
            <div id="mainmenu">
            <ul>
                <li><a href="/home">Home</a></li>
                <li class="active"><a href="/exhibition">Exhibition Hall</a></li>    
            </ul>
            
        </div>
        </div>
    </div>
</header>