@extends('hall.index')

@section('title', 'HOME || UMKM OnlinExpo')

@section('hall')
    
    <script type="text/javascript">
        var page = 0;
        var total_pages = 239;


        $('#showMore').click(function() {
            page++;
            if(page * 10 < total_pages) {
            loadMore(page);
            $('#showHideButton').show();
            }
        });


        function loadMore(page){
        var getArea = document.getElementById('exhibition-area').value;
        var getPrice = document.getElementById('exhibition-price').value;
        var getText = document.getElementById('exhibition-text').value;
        $.ajax(
        {
            url: '/exhibition/show_project?area=' + getArea + '&price=' + getPrice + '&key=' + getText + '&page=' + page,
            type: "GET",
            beforeSend: function()
            {
                $('.loader').show();
            }
        })
        .done(function(data)
        {
            if (data == '') {
                $('#showHideButton').hide();
            }
            $('.loader').hide();
            $("#show-project-list-filter").append(data);
        });
        }

        function filterByPrice(prices) {
        var getArea = document.getElementById('exhibition-area').value;
        var getText = document.getElementById('exhibition-text').value;
        console.log(prices);
        console.log(getArea);
        console.log(getText);


            $.ajax({
                type: 'POST',
                url: '/exhibition/show_developer?area=' + getArea + '&price=' + prices + '&key=' + getText,
                data: 'area=' + getArea + '&price=' + prices + '&key=' + getText,
                success: function(html) {
                    $('#show-developer-items-filter').html(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '/exhibition/show_project?area=' + getArea + '&price=' + prices + '&key=' + getText,
                data: 'area=' + getArea + '&price=' + prices + '&key=' + getText,
                success: function(html) {
                    $('#show-project-list-filter').html(html);
                    $('#showHideButton').show();
                }
            });

        }

        function filterByArea(areas) {
        var getPrice = document.getElementById('exhibition-price').value;
        var getText = document.getElementById('exhibition-text').value;

        console.log(areas);
        console.log(getPrice);
        console.log(getText);


            $.ajax({
                type: 'POST',
                url: '/exhibition/show_developer?area=' + areas + '&price=' + getPrice + '&key=' + getText,
                data: 'area=' + areas + '&price=' + getPrice + '&key=' + getText,
                success: function(html) {
                    $('#show-developer-items-filter').html(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '/exhibition/show_project?area=' + areas + '&price=' + getPrice + '&key=' + getText,
                data: 'area=' + areas + '&price=' + getPrice + '&key=' + getText,
                success: function(html) {
                    $('#show-project-list-filter').html(html);
                    $('#showHideButton').show();
                }
            });
        }

        function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
            callback.apply(context, args);
            }, ms || 0);
        };
        }


        $("#exhibition-text").keyup(delay(function(e) {
            var getArea = document.getElementById('exhibition-area').value;
            var getPrice = document.getElementById('exhibition-price').value;
            var getText = document.getElementById('exhibition-text').value;

            console.log(getText);
            console.log(getArea);
            console.log(getPrice);

            if (getArea || getPrice || getText == '' || getText) {
            $.ajax({
                type: 'POST',
                url: '/exhibition/show_developer?area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                data: 'area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                success: function(html) {
                    $('#show-developer-items-filter').html(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '/exhibition/show_project?area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                data: 'area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                success: function(html) {
                    $('#show-project-list-filter').html(html);
                    $('#showHideButton').show();
                }
            });
            }

        }, 200));

        var perfEntries = performance.getEntriesByType("navigation");
        if (perfEntries[0].type === "back_forward") {
            location.reload(true);
        }
    </script>

    <script type="text/javascript" src="/assets/js/konva.min.js"></script>
    <script>
        var width = window.innerWidth;
        var height = window.innerHeight;
        var isMobile = width > height ? false : true;
        var newHeight = isMobile ? height - 60 : height;
        var newWidth = (2560 * newHeight) / 1440;


        var stage = new Konva.Stage({
        container: 'exhibition-container',
        width: width,
        height: newHeight,
        });

        var layer = new Konva.Layer();
        var mX = newWidth / 2560;
        var mY = newHeight / 1440;

        var grup = new Konva.Group({
        x: ((newWidth - width) / 2) * -1,
        y: 0,
        draggable: true,
        dragBoundFunc: function (pos) {
            var minX = width - newWidth;
            var minY = newHeight;
            var newX = pos.x <= minX ? minX : pos.x >= 0 ? 0 : pos.x;
            var newY = 0;
            return {
            x: newX,
            y: newY,
            };
        },
        });

        // START CREATE OBJECT AND EVENTS
            // Hall Agung
        var fashion = new Konva.Line({
        fill: 'rgba(255, 51, 0,.001)',
        points: [100 * mX, 750 * mY, 600 * mX, 750 * mY, 600 * mX, 1200 * mY, 100 * mX, 1200 * mY],
        //         a                  c                              d          b
        closed: true,
        });
        fashion.on('tap click', function () {
        window.location.href = '/fashion'
        
        })
            // Hall Agung
        var kuliner = new Konva.Line({
        fill: 'rgba(255, 51, 0,.001)',
        points: [1990 * mX, 750 * mY, 2450 * mX, 750 * mY, 2450 * mX, 1200 * mY, 1990 * mX, 1200 * mY],
        //         a                  c                              d          b
        closed: true,
        });
        kuliner.on('tap click', function () {
        window.location.href = '/kuliner'
        
        })
            // Hall Agung
        var craft = new Konva.Line({
        fill: 'rgba(255, 51, 0,.001)',
        points: [640 * mX, 750 * mY, 1100 * mX, 750 * mY, 1100 * mX, 1200 * mY, 640 * mX, 1200 * mY],
        //         a                  c          c                    d          b
        closed: true,
        });
        craft.on('tap click', function () {
        window.location.href = '/craft'
        
        })
            // Hall Agung
        var jasa = new Konva.Line({
        fill: 'rgba(255, 51, 0,.001)',
        points: [1500 * mX, 750 * mY, 1950 * mX, 750 * mY, 1950 * mX, 1200 * mY, 1500 * mX, 1200 * mY],
        //         a                  c                              d          b
        closed: true,
        });
        jasa.on('tap click', function () {
        window.location.href = '/jasa'
        
        })
            // Hall Agung
    
    
        // DONE CREATE OBJECT AND EVENTS

        var imageObj = new Image();
        imageObj.onload = function () {
        var hallmenu = new Konva.Image({
            x: 0,
            y: 0,
            image: imageObj,
            width: newWidth,
            height: newHeight,
        });

        hallmenu.on('mousedown', function () {
            stage.container().style.cursor = 'pointer';
        });
        hallmenu.on('mouseup', function () {
            stage.container().style.cursor = 'default';
        });
        
        craft.on('mouseenter', function () {
            stage.container().style.cursor = "pointer";
            this.opacity(1);
            layer.draw();
          })
          craft.on('mouseleave', function () {
            stage.container().style.cursor = "default";
            this.opacity(0);
            layer.draw();
          })
        jasa.on('mouseenter', function () {
            stage.container().style.cursor = "pointer";
            this.opacity(1);
            layer.draw();
          })
          jasa.on('mouseleave', function () {
            stage.container().style.cursor = "default";
            this.opacity(0);
            layer.draw();
          })
        fashion.on('mouseenter', function () {
            stage.container().style.cursor = "pointer";
            this.opacity(1);
            layer.draw();
        })
        fashion.on('mouseleave', function () {
            stage.container().style.cursor = "default";
            this.opacity(0);
            layer.draw();
        })
        kuliner.on('mouseenter', function () {
            stage.container().style.cursor = "pointer";
            this.opacity(1);
            layer.draw();
        })
        kuliner.on('mouseleave', function () {
            stage.container().style.cursor = "default";
            this.opacity(0);
            layer.draw();
        })

                
        
        
        // add the shape to the layer

        grup.add(hallmenu);

                grup.add(fashion);
                grup.add(kuliner);
                //grup.add(objHallKerinci);
                //grup.add(objHallKrakatau);
                grup.add(craft);
                grup.add(jasa);
                //grup.add(objHallTambora);
                // grup.add(objHallBromo);
        // grup.add(objHallKerinci);
        // grup.add(objHallKrakatau);
        // grup.add(objHallRinjani);
        // grup.add(objHallSemeru);
        // grup.add(objHallTambora);
        layer.add(grup);
        layer.batchDraw();
        };
        imageObj.src = '/media/settings/tegalega1.jpg';

        // add the layer to the stage
        stage.add(layer);
    </script>
     


          

@endsection
