<div class="exhibition-landing">
    <div class="exhibition-container" id="exhibition-container">

    </div>
    <div class="exhibition-button-container">
        <!-- <a href="#" class="button">Contoh, klik di sini</a> -->
    </div>


    <div class="exhibition-list-view-container">
        <div class="list-view-container">

        <button class="button btn-close-list-view">&times;</button>
        <div class="list-view-box">
            <div class="container">
            <div class="row">
                <div class="col-12">

                {{-- <div class="view-property mobile-view">
                    <h4>3 cara melihat properti</h4>
                    <div class="property-button-container">
                    <div class="property-button">
                        <img src="/assets/images/icon-camera.png" alt="">
                        <span class="text">Foto</span>
                    </div>
                    <div class="property-button">
                        <img src="/assets/images/icon-360.png" alt="">
                        <span class="text">Tampilan gambar 360°</span>
                    </div>
                    <div class="property-button">
                        <img src="/assets/images/icon-video.png" alt="">
                        <span class="text">Video</span>
                    </div>
                    </div>
                </div> --}}
                {{-- <div class="lv-filter">
                    <select name="exhibition-area" id="exhibition-area" onChange="filterByArea(this.value)">
                    <option value="">Area</option>
                    <option value="Tegal">Tegal</option><option value="Tangerang">Tangerang</option><option value="Surabaya">Surabaya</option><option value="Solo">Solo</option><option value="Sidoarjo">Sidoarjo</option><option value="Serang">Serang</option><option value="Semarang">Semarang</option><option value="Samarinda">Samarinda</option><option value="Pontianak">Pontianak</option><option value="Pekanbaru">Pekanbaru</option><option value="Pekalongan">Pekalongan</option><option value="Pangkalpinang">Pangkalpinang</option><option value="Pandaan">Pandaan</option><option value="Mojokerto">Mojokerto</option><option value="Medan">Medan</option><option value="Manado">Manado</option><option value="Malang">Malang</option><option value="Makassar">Makassar</option><option value="Lebak">Lebak</option><option value="Karawang">Karawang</option><option value="Jonggol">Jonggol</option><option value="Jambi">Jambi</option><option value="Jakarta Timur">Jakarta Timur</option><option value="Jakarta Selatan">Jakarta Selatan</option><option value="Jakarta Pusat">Jakarta Pusat</option><option value="Jakarta Barat">Jakarta Barat</option><option value="Gresik">Gresik</option><option value="Depok">Depok</option><option value="Cirebon">Cirebon</option><option value="Cibubur - Cileungsi">Cibubur - Cileungsi</option><option value="Bogor">Bogor</option><option value="Bekasi">Bekasi</option><option value="Batam">Batam</option><option value="Banjarmasin">Banjarmasin</option><option value="Banjarbaru">Banjarbaru</option><option value="Bandung">Bandung</option><option value="Bandar Lampung">Bandar Lampung</option><option value="Bali">Bali</option><option value="Ambon">Ambon</option>                    </select>
                    <select name="exhibition-price" id="exhibition-price" onChange="filterByPrice(this.value)">
                    <option value="">Harga</option>
                    <option value="< 500">< 500 Jt</option>
                    <option value="500-1M">500 Jt - 1 M</option>
                    <option value="> 1M">> 1 M</option>
                    </select>
                    <input type="text" id="exhibition-text" class="button btn-submit" placeholder="cari developer atau project">
                    <!-- <a href="#" class="button btn-submit" id="search-developer">Cari developer atau project</a> -->
                </div>
        --}}
                <div class="lv-content">
                    <div class="lv-header lv-section">
                    {{-- <div class="view-property desktop-view">
                        <h4>3 cara melihat property</h4>
                        <div class="property-button-container">
                        <div class="property-button">
                            <img src="/assets/images/icon-camera.png" alt="">
                            <span class="text">Foto</span>
                        </div>
                        <div class="property-button">
                            <img src="/assets/images/icon-360.png" alt="">
                            <span class="text">Tampilan gambar 360°</span>
                        </div>
                        <div class="property-button">
                            <img src="/assets/images/icon-video.png" alt="">
                            <span class="text">Video</span>
                        </div>
                        </div>
                    </div> --}}
                    {{-- <h3>Grup BCA & Agen Properti</h3>
                    <ul class="list-developer-items">
                        <li><a href="/groups/list"><img src="/assets/images/grup-bca.png"></a></li>
                        <li><a href="/broker"><img src="/assets/images/broker-rekanan.png"></a></li>
                    </ul> --}}
                    </div>

                    <div class="lv-developer-list lv-section">
                    <h3>Kuliner</h3>
                    {{-- <div id="show-developer-items-filter">
                        <ul class="list-developer-items">
                        <li><a href="/developer/view/bogor-raya"><img src="/media/developer/28640601_Bogor_Raya_Residential_Logo.png" alt=""></a></li><li><a href="/developer/view/abdael-nusa"><img src="/media/developer/841159Abdael_nusa_pt-Logo-warna-01.png" alt=""></a></li><li><a href="/developer/view/graha-mukti-indah"><img src="/media/developer/843924Logo_developer.jpg" alt=""></a></li><li><a href="/developer/view/papan-inti-sejahtera"><img src="/media/developer/93119logo_papanintisejahtera.png" alt=""></a></li><li><a href="/developer/view/synthesis"><img src="/media/developer/146733Synthesis_Development-1.png" alt=""></a></li><li><a href="/developer/view/batu-panorama"><img src="/media/developer/975309Logo_batu_panorama.jpg" alt=""></a></li><li><a href="/developer/view/pakuwon"><img src="/media/developer/5253020_logo_pakuwon_group_1.png" alt=""></a></li><li><a href="/developer/view/chalidana-group"><img src="/media/developer/553675LOGO_FIX_CHALIDANA_NEW-02.png" alt=""></a></li><li><a href="/developer/view/wg"><img src="/media/developer/438917wg-40-logo-01.png" alt=""></a></li><li><a href="/developer/view/mutiara"><img src="/media/developer/39466File-1-Logo_Developer_The_Mutiara.png" alt=""></a></li>                        </ul>
                    </div> --}}
                    </div>
                    <div class="lv-developer-list lv-section">
                        <h3>Fashion</h3>
                        {{-- <div id="show-developer-items-filter">
                            <ul class="list-developer-items">
                            <li><a href="/developer/view/bogor-raya"><img src="/media/developer/28640601_Bogor_Raya_Residential_Logo.png" alt=""></a></li><li><a href="/developer/view/abdael-nusa"><img src="/media/developer/841159Abdael_nusa_pt-Logo-warna-01.png" alt=""></a></li><li><a href="/developer/view/graha-mukti-indah"><img src="/media/developer/843924Logo_developer.jpg" alt=""></a></li><li><a href="/developer/view/papan-inti-sejahtera"><img src="/media/developer/93119logo_papanintisejahtera.png" alt=""></a></li><li><a href="/developer/view/synthesis"><img src="/media/developer/146733Synthesis_Development-1.png" alt=""></a></li><li><a href="/developer/view/batu-panorama"><img src="/media/developer/975309Logo_batu_panorama.jpg" alt=""></a></li><li><a href="/developer/view/pakuwon"><img src="/media/developer/5253020_logo_pakuwon_group_1.png" alt=""></a></li><li><a href="/developer/view/chalidana-group"><img src="/media/developer/553675LOGO_FIX_CHALIDANA_NEW-02.png" alt=""></a></li><li><a href="/developer/view/wg"><img src="/media/developer/438917wg-40-logo-01.png" alt=""></a></li><li><a href="/developer/view/mutiara"><img src="/media/developer/39466File-1-Logo_Developer_The_Mutiara.png" alt=""></a></li>                        </ul>
                        </div> --}}
                    </div>
                    <div class="lv-developer-list lv-section">
                        <h3>Craft</h3>
                        {{-- <div id="show-developer-items-filter">
                            <ul class="list-developer-items">
                            <li><a href="/developer/view/bogor-raya"><img src="/media/developer/28640601_Bogor_Raya_Residential_Logo.png" alt=""></a></li><li><a href="/developer/view/abdael-nusa"><img src="/media/developer/841159Abdael_nusa_pt-Logo-warna-01.png" alt=""></a></li><li><a href="/developer/view/graha-mukti-indah"><img src="/media/developer/843924Logo_developer.jpg" alt=""></a></li><li><a href="/developer/view/papan-inti-sejahtera"><img src="/media/developer/93119logo_papanintisejahtera.png" alt=""></a></li><li><a href="/developer/view/synthesis"><img src="/media/developer/146733Synthesis_Development-1.png" alt=""></a></li><li><a href="/developer/view/batu-panorama"><img src="/media/developer/975309Logo_batu_panorama.jpg" alt=""></a></li><li><a href="/developer/view/pakuwon"><img src="/media/developer/5253020_logo_pakuwon_group_1.png" alt=""></a></li><li><a href="/developer/view/chalidana-group"><img src="/media/developer/553675LOGO_FIX_CHALIDANA_NEW-02.png" alt=""></a></li><li><a href="/developer/view/wg"><img src="/media/developer/438917wg-40-logo-01.png" alt=""></a></li><li><a href="/developer/view/mutiara"><img src="/media/developer/39466File-1-Logo_Developer_The_Mutiara.png" alt=""></a></li>                        </ul>
                        </div> --}}
                    </div>
                    <div class="lv-developer-list lv-section">
                        <h3>Jasa</h3>
                        {{-- <div id="show-developer-items-filter">
                            <ul class="list-developer-items">
                            <li><a href="/developer/view/bogor-raya"><img src="/media/developer/28640601_Bogor_Raya_Residential_Logo.png" alt=""></a></li><li><a href="/developer/view/abdael-nusa"><img src="/media/developer/841159Abdael_nusa_pt-Logo-warna-01.png" alt=""></a></li><li><a href="/developer/view/graha-mukti-indah"><img src="/media/developer/843924Logo_developer.jpg" alt=""></a></li><li><a href="/developer/view/papan-inti-sejahtera"><img src="/media/developer/93119logo_papanintisejahtera.png" alt=""></a></li><li><a href="/developer/view/synthesis"><img src="/media/developer/146733Synthesis_Development-1.png" alt=""></a></li><li><a href="/developer/view/batu-panorama"><img src="/media/developer/975309Logo_batu_panorama.jpg" alt=""></a></li><li><a href="/developer/view/pakuwon"><img src="/media/developer/5253020_logo_pakuwon_group_1.png" alt=""></a></li><li><a href="/developer/view/chalidana-group"><img src="/media/developer/553675LOGO_FIX_CHALIDANA_NEW-02.png" alt=""></a></li><li><a href="/developer/view/wg"><img src="/media/developer/438917wg-40-logo-01.png" alt=""></a></li><li><a href="/developer/view/mutiara"><img src="/media/developer/39466File-1-Logo_Developer_The_Mutiara.png" alt=""></a></li>                        </ul>
                        </div> --}}
                    </div>

                    
                    </div>

                    <div class="lv-project-list lv-section load-more-button" id="showHideButton">

                    <p align=center><button type="button" id="showMore">Lihat lebih banyak</button></p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    </div>

                </div>

                </div>
            </div>
            </div>


        </div>

        </div>
    </div>

    <button class="button btn-list-view">
        <span class="icon"><img src="/assets/images/arrow-white.png" alt=""></span>
        <span class="text">Cari UMKM</span>
    </button>

    {{-- <div class="modal" id="instruction-modal" tabindex="-1" aria-labelledby="instruction-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <!-- <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div> -->
            <div class="modal-body">

            <div class="modal-instruction">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <img src="/assets/images/hall-guide.png" alt="">
            </div>
            </div>
            <!-- <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div> -->
        </div>
        </div>
    </div> --}}
</div>