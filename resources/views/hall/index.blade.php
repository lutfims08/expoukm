
<html>
    <head>
        <meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="" />
		<meta name="keywords" content="" />

        <title>UMKM OnlinEXPO</title>

        <!-- load CSS -->
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/slick.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/slick-theme.css" />

        <!-- Load font -->
        <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@700&display=swap" rel="stylesheet">

        <!-- add font awesome css for icons -->
        <link href="/assets/css/fontawesome.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/home.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/app.css" rel="stylesheet" type="text/css" />

        <!-- Load Javascript -->
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.js"></script>

        <link rel="shortcut icon" href="/assets/images/logo.png" />

        <!-- Google Tag Manager -->
        {{-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KP2V2HC');</script> --}}
        <!-- End Google Tag Manager -->
    </head>

    <body>
        <div id="container">
            @include('hall.header')
        
            <!-- ADD MOBILE MENU -->
            @include('hall.menumobile')
            @include('hall.menu')
            @yield('hall')
              
            <script type="text/javascript">
                $(document).on('ready', function () {
                $('#instruction-modal').modal('show');
                $('.btn-close-list-view').on('click', function (e) {
                    e.preventDefault();
                    $('.exhibition-list-view-container').removeClass('active');
                    $('.btn-list-view').removeClass('active');
                    $('.btn-list-view').find('.text').text('Cari UMKM');
                })
                $('.btn-list-view').on('click', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('active');
                    $('.exhibition-list-view-container').toggleClass('active');
                    var btnActive = $(this).hasClass('active');
                    if (btnActive) {
                    $(this).find('.text').text('Kembali ke Hall')
                    } else {
                    $(this).find('.text').text('Cari UMKM')
                    }
                })
                });
            </script> 
        </div>
        
</body>
</html>