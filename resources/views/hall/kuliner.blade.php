@extends('hall.index')

@section('title', 'Kuliner || UMKM OnlinExpo')

@section('hall')

      

    <script type="text/javascript">
      function filterByPrice(prices) {
        var getArea = document.getElementById('exhibition-area').value;
        var getText = document.getElementById('exhibition-text').value;
        console.log(prices);
        console.log(getArea);
        console.log(getText);

        if (getArea || prices) {
          $.ajax({
              type: 'POST',
              url: '/exhibition/show_developer?area=' + getArea + '&price=' + prices + '&key=' + getText,
              data: 'area=' + getArea + '&price=' + prices + '&key=' + getText,
              success: function(html) {
                  $('#show-developer-items-filter').html(html);
              }
          });

          $.ajax({
              type: 'POST',
              url: '/exhibition/show_project?area=' + getArea + '&price=' + prices + '&key=' + getText,
              data: 'area=' + getArea + '&price=' + prices + '&key=' + getText,
              success: function(html) {
                  $('#show-project-list-filter').html(html);
              }
          });
        }

      }

      function filterByArea(areas) {
        var getPrice = document.getElementById('exhibition-price').value;
        var getText = document.getElementById('exhibition-text').value;

        console.log(areas);
        console.log(getPrice);
        console.log(getText);

        if (areas || getPrice) {
          $.ajax({
              type: 'POST',
              url: '/exhibition/show_developer?area=' + areas + '&price=' + getPrice + '&key=' + getText,
              data: 'area=' + areas + '&price=' + getPrice + '&key=' + getText,
              success: function(html) {
                  $('#show-developer-items-filter').html(html);
              }
          });

          $.ajax({
              type: 'POST',
              url: '/exhibition/show_project?area=' + areas + '&price=' + getPrice + '&key=' + getText,
              data: 'area=' + areas + '&price=' + getPrice + '&key=' + getText,
              success: function(html) {
                  $('#show-project-list-filter').html(html);
              }
          });
        }
      }

      function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this, args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function () {
            callback.apply(context, args);
          }, ms || 0);
        };
      }


      $("#exhibition-text").keyup(delay(function(e) {
          var getArea = document.getElementById('exhibition-area').value;
          var getPrice = document.getElementById('exhibition-price').value;
          var getText = document.getElementById('exhibition-text').value;

          console.log(getText);
          console.log(getArea);
          console.log(getPrice);

          if (getArea || getPrice || getText == '' || getText) {
            $.ajax({
                type: 'POST',
                url: '/exhibition/show_developer?area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                data: 'area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                success: function(html) {
                    $('#show-developer-items-filter').html(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '/exhibition/show_project?area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                data: 'area=' + getArea + '&price=' + getPrice + '&key=' + getText,
                success: function(html) {
                    $('#show-project-list-filter').html(html);
                }
            });
          }

      }, 200));

      var perfEntries = performance.getEntriesByType("navigation");
      if (perfEntries[0].type === "back_forward") {
          location.reload(true);
      }
    </script>

    <script type="text/javascript" src="/assets/js/konva.min.js"></script>
    <script>
      var width = window.innerWidth;
      var height = window.innerHeight;
      var isMobile = width > height ? false : true;
      var newHeight = isMobile ? height - 60 : height;
      var newWidth = (2560 * newHeight) / 1440;


      var stage = new Konva.Stage({
        container: 'exhibition-container',
        width: width,
        height: newHeight,
      });

      var layer = new Konva.Layer();
      var mX = newWidth / 2560;
      var mY = newHeight / 1440;


      var grup = new Konva.Group({
        x: ((newWidth - width) / 2) * -1,
        y: 0,
        draggable: true,
        dragBoundFunc: function (pos) {
          var minX = width - newWidth;
          var minY = newHeight;
          var newX = pos.x <= minX ? minX : pos.x >= 0 ? 0 : pos.x;
          var newY = 0;
          return {
            x: newX,
            y: newY,
          };
        },
      });

      // START CREATE OBJECT AND EVENTS

      // Booth Abdael Nusa
            var booth01 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [936 * mX, 238 * mY, 1220 * mX, 303 * mY, 1230 * mX, 443 * mY, 1110 * mX, 596 * mY, 850 * mX, 530 * mY],
        closed: true,
      });
      booth01.on('tap click', function () {
        window.location.href = '/kuliner/test'
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth02 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [1326 * mX, 323 * mY, 1624 * mX, 395 * mY, 1624 * mX, 535 * mY, 1510 * mX, 705 * mY, 1230 * mX, 630 * mY],
        closed: true,
      });
      booth02.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth03 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [1716 * mX, 425 * mY, 2036 * mX, 497 * mY, 2018 * mX, 634 * mY, 1926 * mX, 806 * mY, 1630 * mX, 730 * mY],
        closed: true,
      });
      booth03.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth04 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [2160 * mX, 520 * mY, 2500 * mX, 600 * mY, 2468 * mX, 744 * mY, 2390 * mX, 929 * mY, 2074 * mX, 844 * mY],
        closed: true,
      });
      booth04.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth05 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [690 * mX, 523 * mY, 988 * mX, 601 * mY, 1002 * mX, 744 * mY, 869 * mX, 919 * mY, 589 * mX, 841 * mY],
        closed: true,
      });
      booth05.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth06 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [1095 * mX, 630 * mY, 1411 * mX, 713 * mY, 1414 * mX, 854 * mY, 1292 * mX, 1039 * mY, 997 * mX, 956 * mY],
        closed: true,
      });
      booth06.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth07 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [1510 * mX, 734 * mY, 1845 * mX, 825 * mY, 1840 * mX, 970 * mY, 1730 * mX, 1168 * mY, 1420 * mX, 1078 * mY],
        closed: true,
      });
      booth07.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth08 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [1978 * mX, 860 * mY, 2336 * mX, 953 * mY, 2313 * mX, 1097 * mY, 2220 * mX, 1302 * mY, 1886 * mX, 1209 * mY],
        closed: true,
      });
      booth08.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth09 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [436 * mX, 819 * mY, 749 * mX, 911 * mY, 772 * mX, 1051 * mY, 621 * mX, 1249 * mY, 326 * mX, 1159 * mY],
        closed: true,
      });
      booth09.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
            var booth10 = new Konva.Line({
        fill: 'rgba(255, 255, 102,0.4)',
        opacity: 0,
        points: [862 * mX, 938 * mY, 1192 * mX, 1033 * mY, 1209 * mX, 1181 * mY, 1069 * mX, 1391 * mY, 759 * mX, 1296 * mY],
        closed: true,
      });
      booth10.on('tap click', function () {
        window.location.href = ''
        // alert('REDIRECT TO HALL AGUNG')
      })
      

      // var booth2 = new Konva.Line({
      //   fill: 'rgba(255, 255, 102,0.4)',
      //   points: [1326, 323, 1624, 395, 1624, 535, 1510, 705, 1230, 630],
      //   closed: true,
      // });
      // booth2.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth3 = new Konva.Line({
      //   fill: 'rgba(255, 255, 102,0.4)',
      //   points: [1716, 425, 2036, 497, 2018, 634, 1926, 806, 1630, 730],
      //   closed: true,
      // });
      // booth3.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth4 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [2160, 520, 2500, 600, 2468, 744, 2390, 929, 2074, 844],
      //   closed: true,
      // });
      // booth4.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth5 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [690, 523, 988, 601, 1002, 744, 869, 919, 589, 841],
      //   closed: true,
      // });
      // booth5.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })

      // // Booth 6
      // var booth6 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [1095, 630, 1411, 713, 1414, 854, 1292, 1039, 997, 956],
      //   closed: true,
      // });
      // booth6.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth7 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [1510, 734, 1845, 825, 1840, 970, 1730, 1168, 1420, 1078],
      //   closed: true,
      // });
      // booth7.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth8 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [1978, 860, 2336, 953, 2313, 1097, 2220, 1302, 1886, 1209],
      //   closed: true,
      // });
      // booth8.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth9 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [436, 819, 749, 911, 772, 1051, 621, 1249, 326, 1159],
      //   closed: true,
      // });
      // booth9.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })


      // var booth10 = new Konva.Line({
      //   fill: 'rgba(255,255,255,.001)',
      //   points: [862, 938, 1192, 1033, 1209, 1181, 1069, 1391, 759, 1296],
      //   closed: true,
      // });
      // booth10.on('tap click', function () {
      //   window.location.href = '/developer/project/awani-residence'

      // })

      // DONE CREATE OBJECT AND EVENTS

      var imageObj = new Image();
      imageObj.onload = function () {
        var hallAgung = new Konva.Image({
          x: 0,
          y: 0,
          image: imageObj,
          width: newWidth,
          height: newHeight,

        });

        // add the shape to the layer
                  booth01.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth01.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth02.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth02.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth03.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth03.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth04.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth04.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth05.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth05.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth06.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth06.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth07.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth07.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth08.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth08.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth09.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth09.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
                  booth10.on('mouseenter', function () {
          stage.container().style.cursor = "pointer";
          this.opacity(1);
          layer.draw();
        })
        booth10.on('mouseleave', function () {
          stage.container().style.cursor = "default";
          this.opacity(0);
          layer.draw();
        })
        
        // add hall background
        grup.add(hallAgung);

        // add booth objects
                  grup.add(booth01);
                  grup.add(booth02);
                  grup.add(booth03);
                  grup.add(booth04);
                  grup.add(booth05);
                  grup.add(booth06);
                  grup.add(booth07);
                  grup.add(booth08);
                  grup.add(booth09);
                  grup.add(booth10);
                // grup.add(booth2);
        // grup.add(booth3);
        // grup.add(booth4);
        // grup.add(booth5);
        // grup.add(booth6);
        // grup.add(booth7);
        // grup.add(booth8);
        // grup.add(booth9);
        // grup.add(booth10);

        layer.add(grup);
        layer.batchDraw();
      };
      imageObj.src = '/media/hall/77975BOOTH_Hall_A_-_AGUNG_7_.jpg';

      // add the layer to the stage
      stage.add(layer);
    </script>
           

        
    @endsection