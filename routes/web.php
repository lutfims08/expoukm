<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('hall.index');
// });
Route::get('/',[App\Http\Controllers\HallController::class, 'index']);
Route::get('/kuliner',[App\Http\Controllers\HallController::class, 'hallkuliner']);
Route::get('/kuliner/test',[App\Http\Controllers\HallController::class, 'bothtest']);
Route::get('/kuliner/test/1',[App\Http\Controllers\HallController::class, 'bothtest1']);
Route::get('/hall',[App\Http\Controllers\HallController::class, 'hall']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
